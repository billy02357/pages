+++
title = "Home"
date = 2021-12-16
+++

```bash
Welcome! :D

I am Billy02357, or that's how they call me.

I love computers, because I can tell them what to do and they do it.
I love programming, specially in C and C-like programming languages
like Go and Dlang.

I also have some interest in Rust but I can't say weather
I like it or not.

I like Unix-like operating systems, I run Arch GNU/Linux.

I'm a Free Software advocate and I also write Free Software
in my free time, most of the time for learning purposes.
```

Links
=====
[GNU/Linux](https://www.gnu.org/gnu/gnu-linux-faq.html)
[Arch GNU/Linux](https://archlinux.org)
[About Unix](https://en.wikipedia.org/wiki/Unix)
[Golang](https://golang.org)
[Dlang](https://dlang.org)
[Free Software](https://www.fsf.org/)

[Go back](..)
