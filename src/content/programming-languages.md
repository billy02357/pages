+++
title = "Programming Languages"
date = 2021-12-16
+++

C
===
```c
C is probably the best language to learn first, as well as probably
the best in everything else as a general purpose programming language.
It is undeniable how reliable and fast C can be, if written properly.
C is enough low level to allow you to be more in control, but enough
high level to be able to actually program useful programs.
```

C++
===
```cpp
```

Rust
====
```rust
```

Go
===
```go
```

D
===
```d
```

Links
=====

[Golang](https://golang.org)
[Dlang](https://dlang.org)
[Rust](https://rust-lang.org)

[Go back](..)
