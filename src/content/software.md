+++
title = "Software"
date = 2021-12-16
+++

```bash
Personally, I prefer software written in C, because
I can understand it and change it. But Go, C++, Dlang
and even Rust are fine programming languages too.

I personally like simple software
(say suckless-like) because
that's how things should be, right? Simple yet powerful.

I prefer Free Software over all other kinds. That is, software
which enables freedoms 0, 1, 2 and 3 (see Four Freedoms)
```

Links
=====

[Four Freedoms](https://www.gnu.org/philosophy/po/free-sw.ml-en.html#four-freedoms)
[Suckless](https://suckless.org)
[Golang](https://golang.org)
[Dlang](https://dlang.org)
[Rust](https://rust-lang.org)

[Go back](..)
